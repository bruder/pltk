package Text::Toolkit::PLTK::Types;

#  PODNAME: Text::Toolkit::PLTK::Types
# ABSTRACT: Types for PLTK

use true;
use strict;
use warnings;
use utf8::all;

use Moose::Util::TypeConstraints;
use MooseX::Types::Moose -all;
use MooseX::Types -declare => [qw(
    WordCountEngine
    PositiveInt
    TokenType
)];

# Define this alias here, so we can exchange the engine at a later point,
# might it be needed.
class_type WordCountEngine, { class => 'Text::Toolkit::PLTK::Class::WordCount' };

subtype PositiveInt,
     as Int,
  where { $_ > 0 },
message { "$_ is not positive." };

subtype TokenType,
     as Str,
  where { /^\w+$/ },
message { "$_ is not a Token." };

=begin wikidoc

= SYNOPSIS

Types used in PLTK

=end wikidoc

=cut
