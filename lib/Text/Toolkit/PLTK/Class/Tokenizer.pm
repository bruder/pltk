package Text::Toolkit::PLTK::Class::Tokenizer;

#  PODNAME: Text::Toolkit::PLTK::Class::Tokenizer
# ABSTRACT: Tokenizing in PLTK

use Moose;
use Text::Toolkit::PLTK::Syntax;

with 'MooseX::Log::Log4perl';

use File::Slurp;

has input_file => (
    is              => 'rw',
    isa             => File,
    required        => 1,
    # trigger => '_build_input', # ?
);

has input => (
    is              => 'rw',
    isa             => Str,
    lazy            => 1,
    default         => method {
        my $text = File::Slurp::read_file( $->input_file );
        $text
    },
);

has tokens => (
    is              => 'rw',
    isa             => ArrayRef[TokenType],
    lazy            => 1,
    traits          => ['Array'],
    handles         => {
        token_count     => 'count',
        token_elements  => 'elements',
    },
    default         => method {
        [ $->tokenize(text => $->input) ]
    },
);

has word_separator => (
    is              => 'rw',
    isa             => RegexpRef,
    lazy            => 1,
    default         => method { qr/[\s+\p{ispunct}]/ },
    documentation   => q{
        split at spaces (and also remove punctation)
    }
);

has output => (
    is              => 'rw',
    isa             => Num,
    lazy            => 1,
    default         => method {
        $->token_count
    }
);

# Returns the words in the text
# Counting them is up to someone else.
method tokenize(Str :$text) {
    split $->word_separator => $text
}

=begin wikidoc

= SYNOPSIS

Tokenizing in PLTK

= OVERVIEW

For now, this is just a stub

= TODO
It might be wise to create bindings to the C sources that come with your operating system.

=end wikidoc

=cut
