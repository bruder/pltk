package Text::Toolkit::PLTK::Class::WordCount;

#  PODNAME: Text::Toolkit::PLTK::Class::WordCount
# ABSTRACT: Word count algorithm in PLTK

use Moose;
use Text::Toolkit::PLTK::Syntax;

with 'MooseX::Log::Log4perl';

use File::Slurp;

has input_file => (
    is              => 'rw',
    isa             => File,
    required        => 1,
    # trigger => '_build_input', # ?
);

has input => (
    is              => 'rw',
    isa             => Str,
    lazy            => 1,
    default         => method {
        my $text = read_file( $->input_file );
        $text
    },
);

has word_separator => (
    is              => 'rw',
    isa             => RegexpRef,
    lazy            => 1,
    default         => method {
        qr/\s+/
    }
);

has output => (
    is              => 'rw',
    isa             => Num,
    lazy            => 1,
    default         => method {
        scalar $->wc(text => $->input)
    }
);

# Returns the words in the text
# Counting them is up to someone else.
method wc(Str :$text) {
    split $->word_separator, $text
}

=begin wikidoc

= SYNOPSIS

Word count in PLTK

= OVERVIEW

For now, this is just a stub

= TODO
It might be wise to create bindings to the C sources that come with your operating system.

=end wikidoc

=cut
