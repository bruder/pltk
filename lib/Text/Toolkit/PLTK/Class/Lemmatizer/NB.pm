package Text::Toolkit::PLTK::Class::Lemmatizer::NB;

#  PODNAME: Text::Toolkit::PLTK::Class::Lemmatizer::NB
# ABSTRACT: [Abstract for Lemmatizer::NB]

use Moose;
use Text::Toolkit::PLTK::Syntax;

with 'MooseX::Log::Log4perl';

has input_file => (
    is          => 'rw',
    isa         => File,
    required    => 1,
    # trigger => '_build_input', # ?
);

has input => (
    is          => 'rw',
    isa         => Str,
    lazy        => 1,
    default     => method {
        my $text = read_file( $->input_file );
        $text
    },
);

has output => (
    is          => 'rw',
    isa         => Num,
    lazy        => 1,
    default     => method {
        return scalar $->lemmatize(text => $->input)
    }
);

method lemmatize(Str :$word) {

}

=begin wikidoc

= SYNOPSIS

[Short description of Lemmatizer::NB]

= OVERVIEW

For now, this is just a stub

= TODO

Here's more to tell

=end wikidoc

=cut
