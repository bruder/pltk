package Text::Toolkit::PLTK::Role::Application;

#  PODNAME: Text::Toolkit::PLTK::Role::Application
# ABSTRACT: Define application wide settings and attributes

use Moose::Role;
use Text::Toolkit::PLTK::Role::Syntax;
with 'Text::Toolkit::PLTK::Role::Application::Settings';
with 'Text::Toolkit::PLTK::Role::Application::Attributes';

=begin wikidoc

= SYNOPSIS

This package defines application-wide settings and attributes

=end wikidoc

=cut