package Text::Toolkit::PLTK::Role::Logger;

#  PODNAME: Text::Toolkit::PLTK::Role::Logger
# ABSTRACT: Centralized logging in PLTK

use Moose::Role;
use Text::Toolkit::PLTK::Role::Syntax;

use Log::Log4perl ':easy';

BEGIN { Log::Log4perl->easy_init() };

=begin wikidoc

= SYNOPSIS

Centralized logging in PLTK

=end wikidoc

=cut
