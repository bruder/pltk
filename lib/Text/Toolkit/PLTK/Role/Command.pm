package Text::Toolkit::PLTK::Role::Command;

#  PODNAME: Text::Toolkit::PLTK::Role::Command
# ABSTRACT: Common attributes shared by all commands.

use Moose::Role;
use Text::Toolkit::PLTK::Role::Syntax;

requires 'execute';
requires 'abstract';
requires 'description';

has _result => (
    is              => 'rw',
    lazy            => 1,
    builder         => '_build_result',
    documentation   => q{The result.},
);


=begin wikidoc

= SYNOPSIS

Common attributes shared by all commands.

=end wikidoc

=cut
