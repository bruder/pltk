package Text::Toolkit::PLTK::Role::Application::Settings;

#  PODNAME: Text::Toolkit::PLTK::Role::Application::Settings
# ABSTRACT: Define application wide settings

use Moose::Role;
use Text::Toolkit::PLTK::Role::Syntax;

### Sub-Command options (see: https://metacpan.org/module/App::Cmd)

sub allow_any_unambiguous_abbrev { 1 }
# sub set_global_options { }
# sub usage_desc { }


=begin wikidoc

= SYNOPSIS

This package defines application-wide settings, such as App::Cmd's settings.

=end wikidoc

=cut