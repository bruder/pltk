package Text::Toolkit::PLTK::Role::Application::Attributes;

#  PODNAME: Text::Toolkit::PLTK::Role::Application::Attributes
# ABSTRACT: Define application wide attributes

use Moose::Role;
use Text::Toolkit::PLTK::Role::Syntax;

use File::Slurp;

### Commonly shared attributes for all sub-commands

has input_file => (
    is              => 'rw',
    isa             => File,
    required        => 1,
    default         => '-',
    traits          => [ 'Getopt' ],
    cmd_aliases     => [ 'input', 'file' ],
    documentation   => q{File to read from. Use '-' if you want to read from @ARGV, i.e. pipe input},
);

has input_file_contents => (
    is              => 'rw',
    isa             => Maybe[Str],
    lazy            => 1,
    builder         => '_build_input_file_contents',
    traits          => [ 'NoGetopt' ],
    documentation   => q{The result of reading the file (or STDIN). Maybe undef.},
);

method _build_input_file_contents {
    read_file($->input_file) or ''
}

=begin wikidoc

= SYNOPSIS

This package defines application-wide attributes, available to all sub-commands.

=end wikidoc

=cut