package Text::Toolkit::PLTK::Role::Syntax;

#  PODNAME: Text::Toolkit::PLTK::Role::Syntax
# ABSTRACT: Common Syntax for all Roles.

use Syntax::Collector -collect => q/
    use true                                    0;
    use autodie                                 0;
    use invoker                                 0;
    use utf8::all                               0;
    use namespace::autoclean                    0;

    use MooseX::Types::Moose                    0 -all;
    use Text::Toolkit::PLTK::Types              0 ':all';

    use MooseX::Method::Signatures              0;

    use MooseX::AlwaysCoerce                    0;
    use MooseX::Types::Path::Class              0 qw(Dir File);
/;

# use MooseX::OmniTrigger                   0;
# use MooseX::MultiMethods                  0;

=begin wikidoc

= SYNOPSIS

Common Syntax for all Roles.

=end wikidoc

=cut

1;