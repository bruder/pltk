package Text::Toolkit::PLTK::Manual::FAQ;

#  PODNAME: Text::Toolkit::PLTK::Manual::FAQ
# ABSTRACT: PLTK Documentation: FAQ

use true;

=begin wikidoc

= INSTALLATION / FAQ

Q: Moose?
A: Klar Moose :)))

Q: Wo?
A: Wenn du das hier sehen kannst bist du bereits als Developer eingetragen im cip.ifi-gitlab unter: `git@gitlab.cip.ifi.lmu.de:bruder/pltk.git`

Q: Wie loslegen?
A:

* Schau doch einfach mal in den Branch... `feature/base-structure-pltk-classes` ...dort hab ist bereits einen mini-word-count als Exempel implementiert
* Mach dir einfach einen branch auf, dann kann ja auch nix kaputt gehen.
* Wenn du fragen hast: jederzeit – bin sehr gespannt, was da durchdachtes kommt!
* Bei Strukturänderungswünschen natürlich genauso, klar.

Q: Dependencies?
A: 'n Haufen ;-)
Voraussetzung ist eine gute, saubere cpanm-Konfiguration (und Zeit)

in `bin/` gibt es die executable zum Projekt, versuch mal dieses, 
aber zuerst aber mal (wahrscheinlich) dependencies (nach)installieren,
dabei mindestens(!) folgende:

* Dist::Zilla
* das, was beim Kommando `dzil authordeps` rauskommt (dann kannst du
nämlich `dzil build`, `dzil test`, `dzil upload` (to CPAN), etc. in
diesem Projekt verwenden)
* das, was beim Kommando `dzil listdeps` rauskommt (das sind die
Module des Projekts, die durch Dist::Zilla::Plugin::Autoprereqs (oder
so ähnlich) gefunden werden können – durchaus die meisten, aber nicht
zwingend alle)

Sag mal, wie's dir damit geht, ob du dich zurechtfindest, was du von
der Struktur bisher denkst, Vorschläge, Änderungswünsche,
Funktionalität... – her damit ;)!

=end wikidoc

=cut