package Text::Toolkit::PLTK::Manual::PROJECT;

#  PODNAME: Text::Toolkit::PLTK::Manual::PROJECT
# ABSTRACT: PLTK Documentation: PROJECT

use true;

=begin wikidoc

Here is a project structure which is up to discussion:

= Namespaces

Structure:

PLTK::
      Parse(r)::Algorithm::Chart::Earley
                                ::CKY
                                ::Insel
      Parse(r)::Algorithm::TopDown::Backtracking::TopDownBacktracking
      Parse(r)::Text::
      Parse(r)::Log::IRC                                            (gibt's ein Modul, kann man wrappen)
      Parse(r)::Log::Unix::Proc                                     (gibt's ein Modul, kann man wrappen)
      Parse(r)::Email::Header
      Tag(ger)::
      Tag(ger)::POS
      Recognize::EOS
      Recognize::NamedEntities::City
      Recognize::NamedEntities::Person
      Recognize::NamedEntities::ZipCode
      Recognize::NamedEntities::ZipCode::German
      Recognize::NamedEntities::ZipCode::German::OldFashion
      Recognize::NamedEntities::ZipCode::German::CurrentFashion
      Generator::Random::Lebensweisheit
      Generate::Random::Lebensweisheit
      Translate::Statistical    # uses Resource::Lexicon
      Algorithm::Tokenize
      Algorithm::Ngram 
      Algorithm::Wrapped::Ngram::Name-von-gewrapptem-modul1 
      Algorithm::Wrapped::Ngram::Name-von-gewrapptem-modul2 
      Doc::Wrapped::Ngram::Name-von-gewrapptem-modul1::Description
      Doc::Wrapped::Ngram::Name-von-gewrapptem-modul2::Rationale
      Doc::Wrapped::Ngram::ModuleComparisons 
      Doc::Wrapped::Ngram::ProsCons 
      Doc::Contributing 
      Doc::Contributing::CodeStyle 
      Doc::Contributing::GeneralAdvice                               
      Doc::Contributing::Basics
      Doc::Contributing::Moose 
      Doc::Contributing::Basics::Moose 
      Doc::Contributing::Specs 
      Doc::Contributing::MoreInformation 
      Doc::Contributing::MoreInformation::OnRecognizers 
      Doc::Contributing::MoreInformation::OnRecognizers::GeneralOutline 
      Algorithm::WordFrequency 
      FSA::DeterministicFSA::Algorithm
      FSA::DAWG
      FSA::CDAWG
      FSA::SCDAWG
      FSA::SCDAWG::Binding::CPP
      (Resource::)Lexicon::German
      (Resource::)Lexicon::German::City                                    
      (Resource::)Lexicon::German::ZipCode                                    
      (Resource::)Lexicon::German::Noun
      (Resource::)Lexicon::German::Verb
      (Resource::)Grammar::German::FlightBooking
      (Resource::)Grammar::German::HotelReservation
      (Resource::)Grammar::German::StandardStuff?
      (Resource::)Grammar::German::Specify
      (Resource::)Grammar::German::Specify::ContextFree::NonRecursive
      (Resource::)Grammar::German::ContextFree::NonRecursive::FlightReservation
      (Resource::)Grammar::deDE::ContextFree::NonRecursive::FlightReservation
      (Resource::)Grammar::atDE::ContextFree::NonRecursive::MealOrder
      Tutorial::Parsing::ChartParsing::Algorithm::Earley
      Tutorial::Parsing::ChartParsing::Algorithm::CKY
      Bindings::(External::(Resource/Framework)?)?::GrammaticalFramework
      Bindings::(External::(Resource/Framework)?)?::NLTK
      Bindings::FSA::SCDAWG
      (Algorithm::?)PMS::FlexiblePatternMatchingWithStrings:....
      (Algorithm::?)HMM::HiddenMarkovModels
      (Algorithm::?)OCR::Medieval::Binding::Neumann.bizRecognition
      (Algorithm::?)PlagiatsDetektor::David
      (Algorithm::?)PlagiatsDetektor::Foo
      Algorithm::Generate::Random::Scientific::Article                   (gibt's ein Modul, kann man wrappen)
      Generate::Random::Scientific::Article                   (gibt's ein Modul, kann man wrappen)
      Statistical::
      Heuristic::

=end wikidoc

=cut