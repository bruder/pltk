package Text::Toolkit::PLTK::Manual::GETTINGSTARTED;

#  PODNAME: Text::Toolkit::PLTK::Manual::GETTINGSTARTED
# ABSTRACT: PLTK Documentation: GETTINGSTARTED

use true;

=begin wikidoc

= GETTING STARTED

Hier eine Zusammenfassung, wie man zum Projekt beitragen kann.

Das wichtigste wird folgendes sein:

* eine saubere {perlbrew}-Installation
* eine saubere {cpanm}-Konfiguration
* eine saubere {Dist::Zilla}-Installation
* Installation der Module, die man als Anwender braucht, um {pltk} zu verwenden
* Installation der Module, die man braucht, wenn man als Entwickler tätig sein will.

Im folgenden werden für jeden Punkt sowohl die Hintergründe und der Nutzen, als auch Installationhinweise angegeben.

== perlbrew

=== Vorteile

Mit {perlbrew}[1] lassen sich

[1]: https://metacpan.org/module/perlbrew

* neue Versionen von Perl sehr einfach *im userspace*, d.h. nicht *global* (per {sudo}), sondern *lokal* (nur für diesen User) installieren -- neues Perl 5.18 gefällig: nichts leichter als das {perlbrew install perl 5.18.1}
* Perl-Module für jedes Perl einzeln und ebenfalls lokal installieren
* Darüber hinaus Perl-Module *für ein bestimmtes Projekt* installieren.
* {perlbrew} kann noch vieles mehr, aber das ist jetzt nicht wichtig.

=== Installation

Die Installation ist denkbar einfach:

{\curl -L http://install.perlbrew.pl | bash}

Zur Frage, was der Backslash dort vor dem {curl} verloren hat:

der Backslash vor einem shell-Kommando stellt sicher, dass das *originale* Kommando (und nicht etwa eine Überladung dessen (z.B. {function curl { ... }} in irgendeiner {bashrc} oder {zshrc}) oder ein {alias curl=some_magic_curl}) aufgerufen wird.

Danach wird schlicht das Shell-Skript dass sich hinter {http://install.perlbrew.pl} verbirgt, ausgeführt, und dann ist {perlbrew} auch schon installiert.

Soweit so gut.

Nachdem {perlbrew} installiert ist, würde ich als nächsten Schritt empfehlen sofort {cpanm} *durch* {perlbrew} zu installieren -- so harmonieren die beiden am besten miteinander:

Kurz noch einmal prüfen, ob {perlbrew} vorhanden ist (ansonsten neue Shell öffnen oder {~/.bashrc} bzw. {~/.zshrc} neu laden):

{which perlbrew}

Wenn perlbrew aktiv ist, {cpanm} installieren lassen:

{perlbrew install-cpanm}

Wieder check ob cpanm aktiv ist (andernfalls neue shell starten):
{which cpanm} sollte {~/perl5/perlbrew/bin/cpanm} ergeben...

Als nächstes am besten gleich ein frisches, neues Perl installieren mit:

{perlbrew available}

Danach das gewünschte neue Perl mit {perlbrew install perl-5.xx.yy} installieren.

(Das ganze lässt sich durch {perlbrew install -j5 perl-5.xx.yy} beschleunigen: {-j5} macht einen parallelen build, wobei {-j5} eine gute Basis bei 4 Kernen ist -- so sind diese am besten ausgelastet)

Zur Sicherheit {perlbrew} sagen, dass man dieses nun in allen neuen Shells standardmäßig verwenden möchte:

{perlbrew switch perl-5.xx.y}

Neue Shell öffnen, schauen, welches Perl aktiv ist:

{which perl}.

Wenn dieses perl nun also in {/perl5/perlbrew/perls/perl-5.xx.y/bin/perl} zum liegen gekommen ist, halt alles bestens geklappt.

== cpanm

{cpanm}[2] ist ein hervorragendes Modul, um Perl-Module zu installieren.

[2]: https://metacpan.org/module/App::cpanminus

Die Installation von cpanm wurde schon in der Installation von {perlbrew} erledigt.

Das schöne an der Kombination von {perlbrew} und {cpanm} ist nun folgendes:

Wenn ich jetzt standardmäßig (wie oben) {perl-5.xx.y} verwende und {cpanm} mit {perlbrew} installiert habe, werden alle Module, die ich ab jetzt installiere, sauber in {~/perl5/perlbrew/perls/perl-5.18.0/lib/site_perl/5.18.0} abgelegt.

Theoretisch könnte ich dieses Verzeichnis nun schlicht löschen, und weg sind die Module.

Noch dazu sind diese Module nur im userspace installiert, d.h. keine Interferenzen mit anderen Usern, kein betteln beim admin, keine wild verstreuten Module (und übrigens: wo liegen denn die man-pages von den Modulen überhaupt bei einer Installation mit {sudo}...?) -- also alles tip-top aufgeräumt.

== Dist::Zilla

{Dist::Zilla}[3] ist ein gutes Helferlein für Modul-Autoren:

[3]: https://metacpan.org/module/Dist::Zilla

{Dist::Zilla} vereinfacht das Erstellen von Modulen und den ganzen Files, die {cpan} benötigt, enorm.

=== Installation

Da {cpanm} nun erfolgreich installiert ist, ist die Installation von {Dist::Zilla} auch ein einfaches:

{cpanm Dist::Zilla}

Da {Dist::Zilla} sehr umfangreich ist, kann das auch wieder eine Weile dauern...

=== Nutzen/Benutzung

Wenn ich nun ein neues Perl-Modul mit {Dist::Zilla} anlegen (und dieses potentiell bei cpan veröffentlichen will), ist der erste Schritt ganz einfach:

{dzil new My::New::Module}

Der erste Schritt sieht dann folgendermaßen aus:

    $ dzil new My::New::Module                                                                                                                                             
    [DZ] making target dir /Users/dbruder/tmp/My-New-Module
    [DZ] writing files to /Users/dbruder/tmp/My-New-Module
    [DZ] dist minted in ./My-New-Module

...und das Modul hat folgende Grundstruktur:

    $ tree My-New-Module                                                                                                                                                   
    My-New-Module
    |-- dist.ini
    `-- lib
        `-- My
            `-- New
                `-- Module.pm

Das neu erstelle {Modul.pm} sieht dann so aus:

    $ cat My-New-Module/lib/My/New/Module.pm
    use strict;
    use warnings;
    package My::New::Module;

    1;

...womit schon einmal das wichtigste für das neue Modul angelegt ist.

Auch zu beachten ist {dist.ini}: Diese Datei wird im folgenden von {Dist::Zilla} verwendet, um alle benötigten Informationen für {My::New::Module} zu verarbeiten.

=== Wichtige Randbemerkung

Die Art und Weise, wie {dzil}/{Dist::Zilla} diesen neuen Stub aufgebaut hat, heisst "Minting". Dieses wird gesteuert durch sogenannte {MintingProfiles}[4]. Das geschieht zwar völlig im Hintergrund und ist in diesem Sinne hier nicht wichtig, aber der wesentliche Punkt ist, *dass man diesen Prozess durch ein eigenes MintingProfile selbst steuern kann* (was wir später auch tun werden).

[4]: https://metacpan.org/module/Dist::Zilla::MintingProfile::Default


== Module von pltk installieren

Als nächtes wäre es schon einmal interessant, sich anzuschauen, was {Dist::Zilla} eigentlich so alles kann:

    $ dzil
    Available commands:

        commands: list the application's commands
            help: display a command's help screen

             add: add modules to an existing dist
      authordeps: list your distribution's author dependencies
           build: build your dist
           clean: clean up after build, test, or install
         install: install your dist
        listdeps: print your distribution's prerequisites
             new: mint a new dist
             nop: do nothing: initialize dzil, then exit
         release: release your dist
             run: run stuff in a dir where your dist is built
           setup: set up a basic global config file
           smoke: smoke your dist
            test: test your dist
           xtest: run xt tests for your dist

...also eine ganze Menge.

An dieser Stelle möchte ich zwei Sachen von ganz oben aus der Liste noch einmal aufgreifen: dort hatte ich gesagt, dass es nötig sei, Module zu installieren, die man als Anwender braucht, um {pltk} zu verwenden *und*
dass es nötig *sein kann* Module zu installieren, die man braucht, wenn man als Entwickler tätig sein will.

Beides wird oben abgedeckt mit den beiden Befehlen {dzil listdeps}: "print your distribution's prerequisites" und {authordeps}: "list your distribution's author dependencies".

Wenn wir uns im pltk-clone befinden, sieht das (in etwa folgendermaßen aus):

Wenn ein User {pltk} verwenden will (d.h. dieses per {cpanm Text::Toolkit::PLTK} installiert), werden ihm genau jene Dependencies von {cpanm} mit-installiert, die er zur erfolgreichen Ausführung von {pltk}, braucht, also wiederum genau jene, die durch {dzil listdeps} abgebildet werden.

Wenn einer an der Distribution mit-arbeiten (und später womöglich {dzil release} oder {dzil test} ausführen will), wird er genau jene Module brauchen, die durch {dizl authordeps} abgebildet werden.




== Dependencies von pltk installieren

Nach {Dist::Zilla} installiert ist, wird es also Zeit, die Dependencies von {pltk} zu installieren:

    cpanm {dzil listdeps}

und

    cpanm {dzil authordeps}


=== Hinweis


Momentan werden die Dependencies von {pltk} automatisch ermittelt (mit Hilfe von {Dist::Zilla::Plugin::AutoPrereqs}), und wegen der neuen fancy Syntax, die im Projekt verwendet wird, werden zum Teil nicht *alle* benötigten Module installiert.

Für den Moment hilft es nichts, als im {pltk}-Ordner {bin/pltk} so lange auszuführen, und die fehlenden Dependencies nachzuinstallieren (und diese in dist.ini zu notieren!), bis wir hier alles sauber zusammengetragen haben.

Nachdem nun alle (und zwar hoffentlich *wirklich alle*) Dependencies installiert sind, kann man schon getrost mit der Entwicklung anfangen, hooray!




== Projektstruktur

Die Projektstruktur sieht folgendermaßen aus (Erklärung folgt inline):

      tree -d
      .
      |-- bin                       # Ausführbares pltk-Programm liegt hier
      |-- lib                       # Hier liegen die PLTK-Module
      |   `-- Text
      |       `-- Toolkit
      |           `-- PLTK
      |               |-- Class     # Hier liegt z.B. die Klasse WordCount.pm
      |               |-- Command   # Commands: für {pltk command -arg1 -arg2}
      |               `-- Role      # Mehr Zeug
      |-- script                    # Hier liegt ein praktisches Skript
      `-- t                         # Hier liegen die Tests
          |-- Class                 # Für die Klassen (s.o.)
          |-- Command               # Für die Kommandos
          `-- etc                   # Hier liegen Test-Dateien, etc.



=== Projektstruktur, genauer

Wie gesagt, in {bin/} liegt das ausführbare {pltk}.

In {lib/} liegen *Klassen* und *Commands*.

Commands sind die Sub-Kommandos von {pltk}, also {pltk wc} (für {WordCount}). Dabei wird die *Klasse* {WordCount} verwendet, um das Kommando auszuführen (siehe Anmerkungen in {HOWTO.md})

Test-Driven-Development wird empfohlen, d.h. du schreibst erst die Tests, welche die *spec* darlegen, wie das Modul funktionieren soll, *und dann erst das Modul* (!), das diese spec erfüllt. Diese Tests finden sich in {t/} wieder (und werden mit {dzil test}  / {dzil xtest} oder {prove} ausgeführt -- dazu an anderer Stelle mehr).



=== Dokumentation

Die Dokumentation des Toolkit wird automatisch angelegt (vorausgesetzt, du schreibst welche) -- {Dist::Zilla} kümmert sich darum (und auch um ordentliches Aussehen derselben).

Dazu kannst du POD (Plain Old Documentation) schreiben, oder auch Markdown verwenden (siehe z.B. {WordCount.pm}).

*Damit der einstieg dazu, neue Klassen und Commands zu schreiben einfacher wird (und eine einheitliche Struktur gewahrt ist), habe ich ein Skript in {script/generate} zur Verfügung gestellt*

Damit lässt ganz einfach eine neue Klasse {Foo} anlegen:

    script/generate Class Foo
    
Damit hast du einen fertigen stub, mit dem du sofort weiterarbeiten kannst (ich sehe gerade, das Generieren eines Test-Stubs schlägt gerade fehl...).

=== Benutzung von git

Ich empfehle sehr, die {git-extras}[5] zu verwenden und schlage folgendes Branching-Model vor:

* Der master branch ist der master branch
* gearbeitet wird auf dev/feature/bug und refactor branches
* dann pull requests und releases

[5]: https://github.com/visionmedia/git-extras

Mit git-extas ist das wirklich einfach:

    pltk@master$              git feature lemmatizer
    pltk@feature/lemmatizer$  [hack hack (möglichst kleine Commits bitte!)]
    pltk@feature/lemmatizer$  git commit -am 'last commit'
    pltk@feature/lemmatizer$  git checkout master
    pltk@feature/lemmatizer$  git feature finish lemmatizer
    # [Jetzt: Pull request anbieten]

Bei Bug-Fixes schlage ich folgendes Modell vor:

    # [In gitlab ist ein issue im bugtracker eröffnet]
    pltk@master$        git bug issue-66
    pltk@bug/issue-66$  [ hack hack ...]   
    pltk@bug/issue-66$  [ letzter commit ]
    pltk@bug/issue-66$  git checkout master
    pltk@master$        git feature finish issue-66
    # [ Pull request ]



== Fazit


Viel Spass und bitte Rückfragen, wenn was unklar ist.

Wir haben dazu u.a.:

* die mailingliste: <pltk-list@lists.ifi.lmu.de>
* und den bug-tracker: <https://gitlab.cip.ifi.lmu.de/bruder/pltk/issues>

=end wikidoc

=cut