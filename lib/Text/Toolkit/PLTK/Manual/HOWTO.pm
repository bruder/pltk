package Text::Toolkit::PLTK::Manual::HOWTO;

#  PODNAME: Text::Toolkit::PLTK::Manual::HOWTO
# ABSTRACT: PLTK Documentation: HOWTO

use true;

=begin wikidoc

= HOWTO

...noch kurz ein paar Stichworte zum allgemeinen Vorgehen...

0 Schreibe deine Klasse unter welchem Namen auch immer (bspw. `Foo::Lemmatizer`)
0 Mache einen Type LemmatizerEngine in Types.pm (ähnlich wie WordCountEngine)
0 Schreibe einen passenden Command `...PLTK::Command::WordCount`
0 Setze dort den `Foo::Lemmatizer` / bzw. die `LemmatizerEngine` ein

Dieses Vorgehen, hat –IMHO– insofern Vorteile, als:

* Du kannst deinen lemmatizer in einer eigenen Klasse schreiben und diesen dann für dein eigenes CPAN-Portfolio verwenden
* diesen Lemmatizer zusätzlich aber auch innerhalb von PLTK zum Einsatz bringen
* die `LemmatizerEngine` bleibt austauschbar (für den Fall, dass ein besserer daherkommt)
* Die Welten zwischen dem `Command` selbst und dem `Lemmatizer` sind sauber getrennt und der `Lemmatizer` bleibt auch an anderen Stellen individuell einsetzbar

Diesbezüglich habe ich gerade ein paar commits gemacht, die es vielleicht wert sind zu betrachten, da sie einiges erklären können anhand des Beispiels `WordCount`.

Zu beachten ist dort aber, dass `WordCount` nicht eine Klasse `BarBaz::WordCount` darstellt, sondern namentlich innerhalb des PLTK-Universums lebt...

=end wikidoc

=cut