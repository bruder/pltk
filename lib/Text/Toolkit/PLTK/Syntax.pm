package Text::Toolkit::PLTK::Syntax;

#  PODNAME: Text::Toolkit::PLTK::Syntax
# ABSTRACT: Common Syntax for all Modules.

use Syntax::Collector -collect =>  q/
    use true                                    0;
    use invoker                                 0;
    use feature                                 0 'say';
    use namespace::autoclean                    0;

    use Moose::Util::TypeConstraints            0;
    use MooseX::Types::Moose                    0 '-all';
    use Text::Toolkit::PLTK::Types              0 ':all';

    use MooseX::Method::Signatures              0;
    use MooseX::StrictConstructor               0;
    use MooseX::AlwaysCoerce                    0;
    use MooseX::OmniTrigger                     0;
    use MooseX::MultiMethods                    0;
    use MooseX::Types::Path::Class              0 qw(Dir File);
    use MooseX::CurriedDelegation               0;

    use MooseX::AutoImmute                      0;
/;

# use MooseX::CoercePerAttribute              0;
# use MooseX::LazyCoercion                    0;


=begin wikidoc

= SYNOPSIS

Common Syntax for all Modules.

=end wikidoc

=cut

1;