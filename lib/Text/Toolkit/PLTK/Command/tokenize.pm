package Text::Toolkit::PLTK::Command::tokenize;

#  PODNAME: Text::Toolkit::PLTK::Action::tokenize
# ABSTRACT: Tokenization routines

use Moose;
use Text::Toolkit::PLTK::Syntax;
extends qw(
    MooseX::App::Cmd::Command
    Text::Toolkit::PLTK
);
with 'Text::Toolkit::PLTK::Role::Command';

sub abstract       { 'Tokenizer' }
sub description    { "Tokenizer help" }

# # # # # # # # # # # # # # # # # #
# TODO
#   * Handle piped input
#   * Multi word tokens on one line
# # # # # # # # # # # # # # # # # #


has '+_result' => (
    isa             => ArrayRef[Str],
    traits          => [ 'Array' ],
    handles         => {
        result_lines => 'elements',
    },
);

has ignore_file => (
    is              => 'rw',
    isa             => Maybe[File],
    traits          => [ 'Getopt' ],
    cmd_aliases     => [ 'ignore' ],
    documentation   => q{
        A file with a list of multi-word units
    }
);

has ignore_contents => (
    is              => 'rw',
    isa             => ArrayRef[Str],
    lazy            => 1,
    builder         => '_build_ignore_contents',
    traits          => [ 'NoGetopt', 'Array' ],
    handles         => {
        ignore_contents_elements => 'elements',
    },
    documentation   => q{A List of multi-word-tokens.}
);

has ignore_rx => (
    is              => 'rw',
    isa             => RegexpRef,
    default         => method {
        qr/(@{[
            join "|",
            sort { $b cmp $a or length $b <=> length $a }
            $self->ignore_contents_elements
           ]})|\s+/;
    }
);

method _build_result {
    [ grep {$_!~/^$/} grep {defined} split /@{[$self->ignore_rx]}/ => $->input_file_contents ]
}

method _build_ignore_contents {
    return ['New York'] unless defined $->ignore_file;
    return read_file($->ignore_file, array_ref => 1, chomp => 1);
}

# implement views!
method execute {
    say for $->result_lines;
}

=begin wikidoc

= SYNOPSIS

tokenize command

=end wikidoc

=cut
