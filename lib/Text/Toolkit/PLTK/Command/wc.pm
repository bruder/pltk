package Text::Toolkit::PLTK::Command::wc;

#  PODNAME: Text::Toolkit::PLTK::Command::wc
# ABSTRACT: Word count

use Moose;
use Text::Toolkit::PLTK::Syntax;
extends qw(
    MooseX::App::Cmd::Command
    Text::Toolkit::PLTK
);
with 'Text::Toolkit::PLTK::Role::Command';

sub abstract       { 'Word counter' }
sub description    { "Word counter help" }

use Text::Toolkit::PLTK::Class::WordCount;

has wc => (
    is              => 'ro',
    isa             => WordCountEngine,
    lazy            => 1,
    default         => method {
        WordCountEngine->new(
            input_file => $self->input_file,
        )
    },
);

### Required methods / attributes
has '+_result' => (
    isa     => PositiveInt,
    traits  => [ 'Counter' ],
);

method _build_result {
    $->wc->output
}

# implement views!
method execute {
    say $->result;
}

=begin wikidoc

= SYNOPSIS

word count command

=end wikidoc

=cut
