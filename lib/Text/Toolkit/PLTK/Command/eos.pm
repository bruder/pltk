package Text::Toolkit::PLTK::Command::eos;

#  PODNAME: Text::Toolkit::PLTK::Command::eos
# ABSTRACT: End-Of-Sentence Recognition

use Moose;
use Text::Toolkit::PLTK::Syntax;
extends qw(
    MooseX::App::Cmd::Command
    Text::Toolkit::PLTK
);
with 'Text::Toolkit::PLTK::Role::Command';

sub abstract       { 'EOS' }
sub description    { 'EOS help' }

has view => (
    is              => 'rw',
    isa             => enum( qw[concordance pos] ),
    traits          => [ 'Getopt' ],
    documentation   => q{Set the view type of your results.},
);

### Required methods / attributes
has '+_result' => (
    isa             => ArrayRef[Str],
    traits          => [ 'NoGetopt', 'Array' ],
    handles         => {
        result_elements => 'elements',
        result_push     => 'push',
    },
);

method _build_result {
    [ grep { /^[.!?]$/ } split //, $->input_file_contents ]
}

# implement views!
method execute {
    say for $->result_elements
}

=begin wikidoc

= SYNOPSIS

eos command

=end wikidoc

=cut
