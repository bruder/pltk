package Text::Toolkit::PLTK;

#  PODNAME: Text::Toolkit::PLTK
# ABSTRACT: pltk -- nltk for perl

use v5.18;

use Moose;
use Text::Toolkit::PLTK::Syntax;

extends 'MooseX::App::Cmd';

with 'MooseX::Log::Log4perl';
with 'MooseX::Getopt::Dashes';

with 'Text::Toolkit::PLTK::Role::Application';
with 'Text::Toolkit::PLTK::Role::Logger';

=begin wikidoc

= SYNOPSIS

This is PLTK, the Perl Language Toolkit.

=end wikidoc

=cut
