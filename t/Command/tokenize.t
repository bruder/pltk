#!/usr/bin/env perl

use v5.14;
use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use constant test_data => "$FindBin::Bin/../etc/";

use Test::More;

use App::Cmd::Tester;
use File::Slurp;

use Text::Toolkit::PLTK;

# # # # # # # # # # # # # # # # # #
# TODO
#   * Test piped input
#
# # # # # # # # # # # # # # # # # #

TEST1: {
    my $test_file_1   = test_data."lorem_ipsum.in.txt";
    my $result_file_1 = test_data."lorem_ipsum.out.txt";

    my $lorem_ipsum_expected_result = read_file($result_file_1);

    my $result_1 = test_app('Text::Toolkit::PLTK' => [ qw(tokenize --file), $test_file_1 ]);

    is($result_1->stdout, $lorem_ipsum_expected_result, 'Output is as expected');
    is($result_1->stderr, '', 'nothing sent to stderr');
    is($result_1->error, undef, 'threw no exceptions');
}

TEST2: {
    my $test_file_2   = "$FindBin::Bin/../etc/new_york.in.txt";
    my $result_file_2 = "$FindBin::Bin/../etc/new_york.out.txt";
    my $new_york_expected_result = read_file($result_file_2);

    my $result_2 = test_app('Text::Toolkit::PLTK' => [ qw(tokenize --file), $test_file_2 ]);

    is($result_2->stdout, $new_york_expected_result, 'Output is as expected');
    is($result_2->stderr, '', 'nothing sent to stderr');
    is($result_2->error, undef, 'threw no exceptions');
}

done_testing