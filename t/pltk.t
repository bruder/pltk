use Test::More;
use Test::Moose;

use Text::Toolkit::PLTK;
use Text::Toolkit::PLTK::Command::tokenize;
# use Text::Toolkit::PLTK::Role::Application;
# use Text::Toolkit::PLTK::Role::Command;

meta_ok 'Text::Toolkit::PLTK', "... Text::Toolkit::PLTK has a ->meta";
does_ok 'Text::Toolkit::PLTK', 'Text::Toolkit::PLTK::Role::Application', "... Text::Toolkit::PLTK::Role::Application does the Baz role";
does_ok 'Text::Toolkit::PLTK::Command::tokenize', 'Text::Toolkit::PLTK::Role::Command', "... Text::Toolkit::PLTK::Command::tokenize does the Text::Toolkit::PLTK::Role::Command role";
has_attribute_ok 'Text::Toolkit::PLTK::Command::tokenize', '_result', "... Text::Toolkit::PLTK::Command::tokenize has the 'result' attribute";

done_testing