#!/usr/bin/env perl

use v5.14;
use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../../lib";
use constant test_data => "$FindBin::Bin/../etc/";

use Test::More;
use App::Cmd::Tester;
use File::Slurp;

use Text::Toolkit::PLTK::Class::Tokenizer;

TEST1: {
    my $t = Text::Toolkit::PLTK::Class::Tokenizer->new(
        input_file => test_data."new_york.in.txt"
    );

    is  ref $t->tokens,
        ref [],
        'tokens is an array';

    is  $t->token_count,
        8,
        'token count is as expected';

    is_deeply [ $t->token_elements ],
              [qw/Today in New York the sun was shining/],
              'token elements are as expected [NOTE: The punctuation was removed]';
}

done_testing

__END__