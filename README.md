ABOUT
-----

This is the stub for the upcoming pltk repo.

You are more than welcome to contribute!

DESCRIPTION
-----------

pltk is supposed to be a shameless clone of nltk, the Natural Language ToolKit Library written in python.

pltk will be similar, except it is written in Perl.

Of course, this is a tremendous opportunity to build a portfolio.

QUESTIONS, ANSWERS, INSTALLATION
--------------------------------

The manual has moved to the `Text::Toolkit::PLTK::Manual::` namespace.

You can find the online manual pages at: <http://www.cip.ifi.lmu.de/~bruder/projects/pltk/manual/>

LICENSE
-------

WTFPL